<?php

function _lsasimilar_admin_form() {
  $form = array();

  $form['lsactfieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types to index'),
  );

  $form['lsactfieldset']['lsasimilar_content_types'] = array(
    '#type' => 'select',
    '#title' => t('Choose content types'),
    '#description' => t('If no item selected, this filter will not apply and all existing content types will be indexed.'),
    '#multiple' => TRUE,
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('lsasimilar_content_types', array()),
  );

  $form['lsachartfieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types to index'),
  );

  $form['lsactfieldset']['lsasimilar_chart_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Chart X size'),
    '#default_value' => variable_get('lsasimilar_chart_width', 500),
  );

  $form['lsactfieldset']['lsasimilar_chart_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Chart Y size'),
    '#default_value' => variable_get('lsasimilar_chart_height', 500),
  );

  return system_settings_form($form);
}

/**
 * Display the graphic
 */
function _lsasimilar_page($mid) {
  $html = '';
  $path = drupal_get_path('module', 'lsasimilar');
  drupal_set_html_head('<script type="text/javascript" src="https://www.google.com/jsapi"></script>');
  drupal_add_js($path . '/js/googlechart.js', 'module');
  drupal_add_css($path . '/css/customtooltip.css');

  require_once('classes/DrupalMatrix.class.php');
  $m = DrupalMatrix::load_matrix($mid);

  // load already computed matrices
  $sql = "SELECT * FROM {lsasimilar_matrix} WHERE pmid = %d";
  $res = db_query($sql, $mid);
  while ($row = db_fetch_object($res)) {
    switch ($row->etype) {
      case 'stem':
        $stem = DrupalMatrix::load_matrix($row->mid);
        break;
      case 'node':
        $node = DrupalMatrix::load_matrix($row->mid);
        break;
      case 'U':
        // stems matrix
        $u = DrupalMatrix::load_matrix($row->mid);
        break;
      case 'V':
        // nodes matrix
        $v = DrupalMatrix::load_matrix($row->mid);
        break;
    }
  }

  // print nodes with highlighted stems
  $count = 0;
  foreach ($node[0] as $i => $nid) {
    $c = db_result(db_query("SELECT COUNT(sid) FROM {lsasimilar_link} WHERE etype = 'node' AND eid = %d", $nid));
    $html .= _highlight_stems($nid) . ' (stems count ' . $c . ')<br />';
    $count += $c;
  }
  $html .= 'In nodes total stems count = ' . $count;


  // print nodes with indexed stems
  $html .= '<br /><br />';
  foreach ($node[0] as $j => $nid) {
    $nodetext = _get_node_indexed_text($nid);
    foreach ($stem as $i => $sid) {
      $sid = $sid[0];
      $s = db_result(db_query('SELECT stem FROM {lsasimilar_stem} WHERE sid = %d', $sid));
      $nodetext = preg_replace('/(' . $s . ')/iu', '<strong style="font-size:' . $m[$i][$j] * 100 . '">$1</strong>', $nodetext);
    }
    $html .= $nodetext . '<br />';
  }

  // print matrices
  //krumo(DrupalMatrix::load_matrix($mid));
  //krumo($stem, $node, $u, $v);die;
  $html .= DrupalMatrix::dump($m);
  $html .= DrupalMatrix::dump($u);
  $html .= DrupalMatrix::dump($v);

  // get the stems coords
  $stems = array();
  foreach ($u as $i => $row) {
    $stems[$i]->x = $row[0];
    $stems[$i]->y = $row[1];
    $stems[$i]->l = db_result(db_query('SELECT stem FROM {lsasimilar_stem} WHERE sid = %d', $stem[$i][0]));
    $stems[$i]->t = 's';
  }
  unset($u, $stem); // free RAM

  // get the nodes coords
  $nodes = array();
  $temp = $v;
  foreach ($temp[0] as $i => $v) {
    $nodes[$i]->x = $v; // $temp[0][$i];
    $nodes[$i]->y = $temp[1][$i];
    $nodes[$i]->l = db_result(db_query('SELECT title FROM {node} WHERE nid = %d', $node[0][$i]));
    $nodes[$i]->t = 'n';
  }
  unset($temp, $node); //free RAM

  // display google scatter chart through vizualization api
  // first, create a through array

  $lsasimilar = array(
    'chart' => array(
      'data' => array_merge($stems, $nodes),
      'width' => variable_get('lsasimilar_chart_width', 500),
      'height' => variable_get('lsasimilar_chart_height', 500),
    ),
    'other' => '',
  );

  drupal_add_js(array('lsasimilar' => $lsasimilar), 'setting');
  unset($stems, $node);

  /*
  $labels = array();
  foreach($stems as $i => $stem) {
    $labels[] = 'stem_' . $i;
    $html .= "data.addRow([$stem->x, $stem->y, null]);\n";
  }

  foreach($nodes as $i => $node) {
    $labels[] = 'node_' . $i;
    $html .= "data.addRow([$node->x, null, $node->y]);\n";
  }

  $html .= "var labels = ['" . implode("', '", $labels) . "']";
  */

  /*
  $html .= <<<JS
JS;
  */

  $html .= '<div id="tooltip" style="height:50px;overflow:hidden;"></div>';
  $html .= '<div id="stems_nodes"></div>';

  return $html;
}



function _lsasimilar_page2() {
  $html = '';
  // display the rows for calc
  $html .= '<pre>';
  $mult = 100;
  // stems
  foreach($stems as $i => $stem) {
    $stem->x = round($stem->x * $mult);
    $stem->y = round($stem->y * $mult);
    $html .= "stem_$i $stem->x $stem->y 1\n";
  }

  // nodes
  foreach($nodes as $i => $node) {
    $node->x = round($node->x * $mult);
    $node->y = round($node->y * $mult);
    $html .= "node_$i $node->x $node->y 2\n";
  }

  return $html;


  /*
  $uri = "http://chart.googleapis.com/chart?chs=600x480&cht=s&chco=FF0000|0000FF&chdl=Stems|Nodes&chd=t:";
  $i = 0;
  $count = 50;
  $ex = array('stems' => array(), 'nodes' => array());
  foreach ($stemsx as $k => $x) {
    if (++$i > $count) break;
    $uri .= round($mult * $x); $ex['stems'][$k] .= round($mult * $x + 300) . ' ';
    $uri .= ',';
    $uri .= round($mult * $nodesx[$k]); $ex['nodes'][$k] .= round($mult * $x + 300) . ' ';
    $uri .= ',';
  }
  $uri = substr($uri, 0, -1);
  $uri .= '|';
  $i = 0;
  foreach ($stemsy as $k => $y) {
    if (++$i > $count) break;
    $uri .= round($mult * $y); $ex['stems'][$k] .= round($mult * $y + 300) . ' ';
    $uri .= ',';
    $uri .= round($mult * $nodesy[$k]); $ex['nodes'][$k] .= round($mult * $y + 300) . ' ';
    $uri .= ',';
  }
  $uri = substr($uri, 0, -1);


  // final processing of $ex array
  foreach ($ex['stems'] as $i => $v) {
    $sql = "SELECT s.stem a
      FROM {lsasimilar_vector} v, {lsasimilar_stem} s
      WHERE v.mid = %d
        AND v.vtype = 'sids'
        AND v.i = %d
        AND v.v = s.sid";
    $res = db_query_range($sql, $mid, $i, 0, 1);
    $ex['stems'][$i] .= db_result($res);

    $sql = "SELECT n.title a
      FROM {lsasimilar_vector} v, {node} n
      WHERE v.mid = %d
        AND v.vtype = 'eids'
        AND v.i = %d
        AND v.v = n.nid";
    $res = db_query_range($sql, $mid, $i, 0, 1);
    $ex['nodes'][$i] .= db_result($res);
  }

  $html = '';
  $html .= '<pre>';
  $html .= var_export($ex, TRUE);
  $html .= '<pre>';
  $html .= "Stems\n";
  foreach ($ex['stems'] as $k => $v) {
    $html .= "$v\n";
  }
  $html .= "Nodes\n";
  foreach ($ex['nodes'] as $k => $v) {
    $html .= "$v\n";
  }
  $html .= '</pre>';


  $html .= $uri . '<br /><br />';
  $html .= '<img src="' . $uri . '" />';
  */
}
