google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawVisualization);

function drawVisualization() {
  var data = new google.visualization.DataTable();
  data.addColumn("number", "X");
  data.addColumn("number", "Stems");
  data.addColumn("number", "Nodes");

  // get the values from Drupal.settings
  var a = Drupal.settings.lsasimilar.chart.data;
  for (var i = 0; i < a.length; ++i) {
    if (a[i].t == 's') {
      data.addRow([+a[i].x, +a[i].y, null]);
    }
    else {
      data.addRow([+a[i].x, null, +a[i].y]);
    }
  }

  // Create and draw the visualization.
  var chart = new google.visualization.ScatterChart(document.getElementById('stems_nodes'));
  chart.draw(data, {title: 'Stems - Nodes distribution',
                width: Drupal.settings.lsasimilar.chart.width,
                height: Drupal.settings.lsasimilar.chart.height,
                vAxis: {title: 'Y', titleTextStyle: {color: 'green'}},
                hAxis: {title: 'X', titleTextStyle: {color: 'green'}}}
  );

  // add custom listeners
  var mouseOver = function(e) {
    $('#tooltip').html(a[e.row].l);
    //$('#stems_nodes').before('<div id="tooltip">' + a[e.row].l + '</div>');
  };
  var mouseOut = function(e) {
    $('#tooltip').html('');
    //$('#tooltip').remove();
  };

  google.visualization.events.addListener(chart, 'onmouseover', mouseOver);
  google.visualization.events.addListener(chart, 'onmouseout', mouseOut);
}
