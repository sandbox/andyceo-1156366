<?php

class DrupalMatrix {
  const DELIMITER = ',';
  const BRACKETS = '[]';
  private $maintable = 'lsasimilar_matrix';
  private $storetable = 'lsasimilar_matrixstore';

  /**
   * Save matrix in database.
   *
   * @param $m
   *   (array) Matrix.
   *
   * @param $matrix_type
   *   (string) Matrix type. The matrices can have types: M (main matrix),
   *   U (stem matrix), S (diagonal matrix), V (entities matrix),
   *   and this types to store vectors: 'stem', 'node'
   *
   * @param $pmid
   *   (int) Parent matrix id. The id of Main matrix.
   *
   * @param $desc
   *   (string) Description for matrix.
   *
   * @return
   *   (int) The matrix id.
   */
  static function save_matrix(&$m, $matrix_type, $pmid = 0, $integers = FALSE, $desc = '') {
    $sql = "INSERT INTO {lsasimilar_matrix} (pmid, etype, integers, description, dt) VALUES (%d, '%s', %d, '%s', %d)";
    db_query($sql, $pmid, $matrix_type, (int)$integers, $desc, time());
    $mid = db_last_insert_id('lsasimilar_matrix', 'mid');

    $sql = "INSERT INTO {lsasimilar_matrixstore} (mid, m, n, v) VALUES ";
    $placeholders = array();
    $values = array();
    foreach ($m as $i => $row) {
      foreach ($row as $j => $v) {
        $placeholders[] = '(%d, %d, %d, %f)';
        $values = array_merge($values, array($mid, $i, $j, $v));
      }
    }
    $sql .= implode(', ', $placeholders);
    db_query($sql, $values);
    return $mid;
  }

  /**
   * Load matrix from database.
   */
  static function load_matrix($mid) {
    $sql = "SELECT integers FROM {lsasimilar_matrix} WHERE mid = %d";
    $integers = db_result(db_query($sql, $mid));
    $sql = "SELECT * FROM {lsasimilar_matrixstore} WHERE mid = %d";
    $res = db_query($sql, $mid);
    $m = array();
    while ($row = db_fetch_object($res)) {
      if ($integers) {
        $m[$row->m][$row->n] = (int)round($row->v);
      }
      else {
        $m[$row->m][$row->n] = $row->v;
      }
    }
    return $m;
  }

  /**
   * Вывод матрицы куда-нибудь в каком-нибудь виде
   */
  static function dump($m) {
    if (is_numeric($m)) {
      $mid = $m;
      $m = self::load_matrix($mid);
      $integers = db_result(db_query('SELECT integers FROM {lsasimilar_matrix} WHERE mid = %d', $mid));
    }

    $result = array();
    foreach($m as $m => $r) {
      if ($integers) {
        $r = array_map(function ($v) {return sprintf('%2d', $v);}, $r);
      }
      else {
        $r = array_map(function ($v) {return sprintf('%10f', $v);}, $r);
      }

      $result[$m] = substr(self::BRACKETS, 0, 1) .
                  implode(self::DELIMITER, $r) .
                  substr(self::BRACKETS, 1, 1);
    }
    $result = '<pre>' . implode("\n", $result) . '</pre>';

    return $result;
  }

  /**
   * Возьмем SVD2D-разложение.
   */
  static function get_svd2d($mid) {
    $objects = array();
    $umid = db_result(db_query("SELECT mid FROM {lsasimilar_matrix} WHERE pmid = %d AND etype  = 'U'", $mid));
    $stemmid = db_result(db_query("SELECT mid FROM {lsasimilar_matrix} WHERE pmid = %d AND etype  = 'stem'", $mid));

    // get only the first two coordinates
    $sql = "SELECT m, n, v FROM {lsasimilar_matrixstore} ms WHERE mid = %d AND m IN (0, 1)";
    $res = db_query($sql, $umid);
    while ($row = db_fetch_object($res)) {

    }
  }

}
